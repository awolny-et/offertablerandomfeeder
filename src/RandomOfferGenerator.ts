import {Channel} from "amqplib";

export class RandomOfferGenerator {
    private lastId = 0;

    async run(minDelay: number, maxDelay: number, channel: Channel, rabbitQueue: string) {
        await new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, minDelay + Math.random() * maxDelay);
        });

        channel.sendToQueue(rabbitQueue, new Buffer(JSON.stringify(
            this.generateSingleOffer()
        )));
    }

    generateSingleOffer() {
        let currentTime = (new Date()).toISOString();

        return {
            "origin_event_time": currentTime,
            "payload": {
                "id": ++this.lastId,
                "currency": "EUR",
                "crypto_currency": "BTC",
                "unit_price": {"base": Math.trunc(10 + Math.random() * 50).toString(), "decimal_points": Math.trunc(Math.random() * 2)},
                "amount": {"base": Math.trunc(Math.random() * 100 + 200).toString(), "decimal_points": Math.trunc(Math.random() * 2)},
                "time": currentTime,
                "type": Math.random() > 0.5 ? "bid" : "ask"
            }
        };
    }
}
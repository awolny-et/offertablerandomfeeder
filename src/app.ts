import {Channel, connect, Connection} from "amqplib";
import {RandomOfferGenerator} from "./RandomOfferGenerator";
import {RandomTransactionGenerator} from "./RandomTransactionGenerator";

let rabbitUrl = process.env.RABBIT_URL || 'amqp://offer-queue/';
let offersQueue = process.env.QUEUE_OFFERS || "offers.queue",
    transactionsQueue = process.env.QUEUE_TRANSACTIONS || "transactions.queue";
let maxDelay = parseInt(process.env.MESSAGES_MAX_DELAY) || 100;
let minDelay = parseInt(process.env.MESSAGES_MIN_DELAY) || 10;

// #1 connect to RabbitMQ
let channelPromise = connect(rabbitUrl)
    .then((amqpConnection: Connection) => {
        return amqpConnection.createChannel();
    });

channelPromise.then(async (channel: Channel) => {
    let offerRandomGenerator = new RandomOfferGenerator();

    while (true) {
        await offerRandomGenerator.run(minDelay, maxDelay, channel, offersQueue);
    }

});

channelPromise.then(async (channel: Channel) => {
    let transactionRandomGenerator = new RandomTransactionGenerator();

    while (true) {
        await transactionRandomGenerator.run(minDelay, maxDelay, channel, transactionsQueue);
    }

});


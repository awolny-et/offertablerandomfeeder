import {Channel} from "amqplib";

export class RandomTransactionGenerator {
    private lastId = 0;

    async run(minDelay: number, maxDelay: number, channel: Channel, rabbitQueue: string) {
        await new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, minDelay + Math.random() * maxDelay);
        });

        channel.sendToQueue(rabbitQueue, new Buffer(JSON.stringify(
            this.generateSingleTransaction()
        )));
    }


    generateSingleTransaction() {
        let currentTime = (new Date()).toISOString();

        return {
            "origin_event_time": currentTime,
            "payload": {
                "id": ++this.lastId,
                "currency": "EUR",
                "crypto_currency": "BTC",
                "unit_price": {
                    "base": Math.trunc(1000 + Math.random() * 5000).toString(),
                    "decimal_points": Math.trunc(Math.random() * 4)
                },
                "amount": {
                    "base": Math.trunc(Math.random() * 100 + 200).toString(),
                    "decimal_points": Math.trunc(Math.random() * 2)
                },
                "time": currentTime,
                "type": Math.random() > 0.5 ? "buy" : "sell"
            }
        };
    }
}
# OfferTable Random Feeder

## Wymagania

1. Node.js >= 8.5
2. Uruchomiona instancja Rabbita, na której nasłuchuje aplikacja [`OfferTable`](https://bitbucket.org/exchangetechnologiesphp/offertable).

## Budowanie projektu

Aby zbudować aplikację ze źródeł, wykonaj:

```bash
npm run build
```

lub używając Docker'a:

```bash
docker-compose run random-generator npm run build
```

Wygenerowane źródła znajdują się w katalogu `dist`.

## Uruchamianie

Aby uruchomić projekt, wystarczy wykonać polecenie:

```bash
npm run start
```

..lub używając Docker'a:

```bash
docker-compose up
```

Należy jednak pamiętać, że w momencie uruchamiania aplikacji serwer RabbitMQ musi działać i być dostępny sieciowo.

W przypadku uruchamiania lokalnego, używając Docker'a, uruchom projekt `offer-table`.

## Parametryzacja uruchomienia

### Zmienne środowiskowe

Wszystkie parametry mają swoją wartość domyślną, przystosowaną do uruchamiania projektu
w kontenerach Docker'owych.


Lista dostępnych ENV:

* `RABBIT_URL` - URI do serwera RabbitMQ, razem z adresem VHosta, domyślnie: `amqp://offer-queue/`
* `QUEUE_OFFERS` - nazwa kolejki na którą aplikacja będzie wrzucać zdarzenia pojawienia się oferty, domyślnie: `offers.queue`
* `TRANSACTIONS_QUEUE` - nazwa kolejki na którą aplikacja będzie wrzucać zdarzenia pojawienia się transakcji, domyślnie: `transactions.queue`
* `MESSAGES_MIN_DELAY` - minimalny czas pomiędzy wysłaniem kolejnych zdarzeń, w milisekundach, domyślnie: `5`
* `MESSAGES_MAX_DELAY` - maksymalny czas pomiędzy wysłaniem kolejnych zdarzeń, w milisekundach, domyślnie: `50`
    
# FAQ

### Uruchomiłem projekt na Dockerze, ale aplikacja nie może połączyć się z Rabbitem.

1. Sprawdź, czy projekt `offer-table` uruchomiony jest z najnowszej wersji branch'a `master`.
2. Sprawdź, jaką nazwę podsieci otrzymał projekt [`OfferTable`](https://bitbucket.org/exchangetechnologiesphp/offertable). Możesz to zrobić wykonując polecenie: `docker network ls` 
Domyślnie powinno być to `offertable_default`, jednak nie ma takiej gwarancji. Jeśli ta nazwa się różni - podmień wystąpienia
nazwy sieci na odpowiednie:

Przykład dla nazwy sieci `abcd`:

```yaml
services:
  random-generator:
    (...)
    networks:
      - default
      - abcd
networks:
  abcd:
    external: true
``` 
